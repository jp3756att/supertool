<?php
$servername = "zlt08585.vci.att.com";
$username = "admin";
$password = "admin123";
$dbname = "NonProdDB";
$conn = "";
$DBConnect = False;

$Type = $_POST['TypeParam'];
$Value = json_decode($_POST['ValueParam']); 
$User = $_POST['user']; 

date_default_timezone_set("America/Los_Angeles");
# Try to connect to the DB
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $DBConnect = True;
}

# If DB connection fails
catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
    $DBConnect = False;
}

if($Type == "Update"){

    foreach ($Value as $element) {
        $property = $element->property;
        $value = $element->value;
        $environment = $element->environment;
        $instance = $element->instance;

        $sqlProperty = "";

        switch ($property){
            case "MuteUntilInput":
                $sqlProperty = "MuteTime";
                break;
            case "PermMuteButton":
                $sqlProperty = "PermMute";
                break;
            case "WebpageButton":
                $sqlProperty = "HasWebpage";
                break;
            case "MessageInput":
                $sqlProperty = "Message";
                break;
            case "DownCheckSelect":
                $sqlProperty = "ConsoleOrAdmin";
                break;
            case "NotifyButton":
                $sqlProperty = "Notify";
                break;
            case "RoomsInput":
                $sqlProperty = "Rooms";
                break;
        }

        print '<script> console.log("Value: ' . $value . '") </script>';

        if ($value == 'false'){
            $value = '0';
        }
        else if ($value == 'true'){
            $value = '1';
        }

        $sql = "UPDATE NonProd_App SET " . $sqlProperty . "='" . $value . "' WHERE Environment='" . $environment . "' and Instance='" . $instance . "' LIMIT 1"; 
        print '<script> console.log("' . $sql . '") </script>';
        if ($conn->query($sql) != FALSE) {
             print '<script> console.log("DB Write Success") </script>';
        } else {
            print '<script> console.log("DB Error: "' . $conn->error . ') </script>';
        }

        $edits = "Property: " . $property . " |  Value: " . $value . " |  Environment: " . $environment . " |  Instance: " . $instance;
        $loggingsql = "INSERT INTO NonProd_Logging (User_ID, Changes, Time_Changed) VALUES ('" . $User  . "' ,'" . $edits . "', '" . date("Y-m-d H:i:s") . "')"; 
        if ($conn->query($loggingsql) != FALSE) {
             print '<script> console.log("DB Write Success") </script>';
        } else {
            print '<script> console.log("DB Error: "' . $conn->error . ') </script>';
        }
        #print '<script> console.log("DB Write Success") </script>';
    }
     
}
else if ($Type == "Message"){

    $message = $Value->message;
    $environment = $Value->environment;
    $instance =  $Value->instance;


    $sql = "UPDATE NonProd_App SET Message='" . $message . "' WHERE Environment='" . $environment . "' and Instance='" . $instance . "' LIMIT 1"; 
    print '<script> console.log("' . $sql . '") </script>';
    if ($conn->query($sql) != FALSE) {
            print '<script> console.log("DB Write Success") </script>';
    } else {
        print '<script> console.log("DB Error: "' . $conn->error . ') </script>';
    }

    $edits = "Message: " . $pmessage . " |  Environment: " . $environment . " |  Instance: " . $instance;
    $loggingsql = "INSERT INTO NonProd_Logging (User_ID, Changes, Time_Changed) VALUES ('" . $User  . "' ,'" . $edits . "', '" . date("Y-m-d H:i:s") . "')"; 
    if ($conn->query($loggingsql) != FALSE) {
            print '<script> console.log("DB Write Success") </script>';
    } else {
        print '<script> console.log("DB Error: "' . $conn->error . ') </script>';
    }
    
}

?>