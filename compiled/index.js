
listofChanges = [];
serverAction = "";
loggedInUser = "";

// A changed element on the page
class changedElement {
  constructor(property, value, environment, instance) {
    this.property = property;
    this.value = value;
    this.environment = environment;
    this.instance = instance;
  }
}

class actionMessage {
    constructor(message, environment, instance) {
        this.message = message;
        this.environment = environment;
        this.instance = instance;
    }
}

// Set the tab color to red if there is a DownTime
function setTabColors(env, downtime, permMute, muteTime, currentElement) {

    if (downtime != "") {
        
        muteDateTime = new Date(muteTime);
        currDate = new Date();
        
        if ( typeof muteTime == 'undefined' || muteTime == "" ){
            muteDateTime = new Date('1999-01-01 01:01:01');
        }

        if ( currDate > muteDateTime && permMute == 0) {
            // Color row red if down
            $('script').last().closest('td').css("background-color", "#ff4d4d");
            $('script').last().closest('tr').prev().css("background-color", "#ff4d4d");
            $('script').last().closest('tr').prev().children('td').css("background-color", "#ff4d4d");

            // Color environment tab red if down
            $('a[href="#' + env + '"]').css({ "border-style": "solid", "border-width": "2px", "border-color": "red" });

            // Color environment group tab red if down
            var envGroup = env.replace(/[0-9]/g, '').toUpperCase();
            $('a[href="#' + envGroup + '"]').css({ "border-style": "solid", "border-width": "2px", "border-color": "red" });
        }
    }
}

// Set all the DIM table information
function SetDIMInfo(info) {

    if(info['MuteTime'] != null) { 
        $(".MuteUntilInput").last().val(info['MuteTime'].replace("/-/g","/").replace(" ", "T"));
        console.log(info['MuteTime'].replace("/-/g","/").replace(" ", "T"));
    }

    if(info['PermMute'] == 1){
        $(".PermMuteButton").last().prop('checked', true);
    }

    if(info['Notify'] == 1){
        $(".NotifyButton").last().prop('checked', true);
    }

    if(info['HasWebpage'] == 1){
        $(".WebpageButton").last().prop('checked', true);
    }

    $(".MessageInput").last().val(info['Message']);

    $(".RoomsInput").last().val(info['Rooms']);

    $(".DownCheckSelect").last().val(info['ConsoleOrAdmin']);

}

// Open deails panel (row)
$(function () {
    $(".DeetsButton").click(function () {
        var InstEnvFull = $(this).parent().parent().attr("class");
        var InstEnv = InstEnvFull.split(" ")[0]
        console.log(InstEnv);
        if ($("." + InstEnv + "Details").hasClass("active")) {
            $("." + InstEnv + "Details").hide();
            $("." + InstEnv + "Details").removeClass("active");
        }
        else if ($("." + InstEnv + "Details").hasClass("active") == false) {
            $("." + InstEnv + "Details").show();
            $("." + InstEnv + "Details").addClass("active");
        }
    });
});

// If and updatable field is changed
$(function () {
    $(".Updatable").change(function() {

        // Get information about property
        var server = $(this).closest(".detailsRow").prev().find(".ServerLink").eq(0).text();
        var environment = $(this).closest(".tab-pane").attr('id');
        var instance = $(this).closest(".detailsRow").prev().find('.InstanceCol').find('a').text();
        var property = $(this).attr('class').split(" ")[0];
        var propertyValue = "";

        // If the element is a checkbox, get value differently than textbox
        if($(this).attr('type') == "checkbox") {
            if($(this).is(':checked')){
                propertyValue = "true";
            }
            else{
                propertyValue = "false";
            }
        }
        else {
            var propertyValue = $(this).val();
        }

        console.log("Environment:" + environment);
        console.log("Instance: " + instance);
        console.log("Property: " + property);
        console.log("Value: " + propertyValue);

        // Create a new changedElement and push it on the array
        listofChanges.push(new changedElement(property, propertyValue, environment, instance));

        // Show the Update Button since there is changes to commit
        $(".UpdateButton").show();
    });
});

// If you click the update button
$(function () {
    $(".UpdateButton").click(function(){
        writeToDB("Update", listofChanges);
        listofChanges = [];
        location.reload()
    });
});

// Open the modal for the server details
$(function () {
    $(".ServerLink").click(function() {
        var ServerName = $(this).text();
        console.log($(this).text());
        $.ajax({
            type: "POST",
            url: "instances-popup.php",
            data: { "ServerName":$(this).text()},
            success: function(response){ 
                $(".modal-body.modalServerInfo").html(response);
                $(".modal-title.modalServerInfo").html("Info about " + ServerName);
            }
        });
    });
});

// Open the modal for the instance details
$(function () {
    $(".InstanceLink").click(function() {
        var Instance = $(this).text();
        var Environment = $(this).closest('.tab-pane').attr('id');
        var splitEnvironment = Environment.split("-");
        var EnvironmentType = splitEnvironment[splitEnvironment.length - 1].toLowerCase();
        var EnvironmentGroup = splitEnvironment[0];
        console.log($(this).prev().text());
        $.ajax({
            type: "POST",
            url: "server-popup.php",
            data: { "Instance":$(this).text(), "Environment":$(this).closest('.tab-pane').attr('id'), "AccountType": accountType },
            success: function(response){ 
                $(".modal-body.modalInstanceInfo").html(response);
                $(".modal-title.modalInstanceInfo").html("Info about " + Environment + " " + Instance + " ");
                console.log("Group:" + EnvironmentGroup);
                if(EnvironmentGroup == "BC"){
                    $(".logsButton").attr("onclick","location.href='http://zltv8387.vci.att.com:5601/app/kibana#/discover?_g=()&_a=(columns:!(_source),filters:!((%27$$hashKey%27:%27object:4060%27,%27$state%27:(store:appState),meta:(alias:!n,disabled:!f,index:%27logstash-23015-*%27,key:environment,negate:!t,value:com-att-bc-" + EnvironmentType + "),query:(match:(environment:(query:com-att-bc-dev1,type:phrase))))),index:%27logstash-23015-*%27,interval:auto,query:(query_string:(analyze_wildcard:!t,query:%27*%27)),sort:!(%27@timestamp%27,desc'");
                }
            }
        });
    });
});

// If you accept the message prompt for a server action (Start, Stop, etc)
$(function () {
    $(".messageAcceptOK").click(function() {
        if( $('#actionMessage').val().length > 6 ){
            actionOnServer(serverAction);
            environment = $('#instanceInfoModal').find(".modal-header").find("h3").text().split(" ")[2];
            instance = $('#instanceInfoModal').find(".modal-header").find("h3").text().split(" ")[3];
            message = $('#actionMessage').val()
            console.log("Environment: " + environment);
            console.log("Instance: " + instance);
            console.log("message: " + message);
            writeToDB("Message", new actionMessage(message, environment, instance) )

        }
    });
});

// Check if the message for a server action is long enough
$(function (){
    $('#actionMessage').keyup(function(){
        if( $('#actionMessage').val().length > 6 ){
            $('.messageAcceptOK').show();
        }
        else{
            $('.messageAcceptOK').hide();
        }
    });

});

// Clear the message for a server action
function clearMessage(){
    $('.messageAcceptOK').hide();
    $('#actionMessage').val("");
};

// Set server action and clear message
function promptForMessage(action){
    $('.messageAcceptOK').hide();
    $('#actionMessage').val("");
    serverAction = action;
};

// Open the modal for the instance details
function actionOnServer(action) {

    var header = $('.restartButton').parent().parent().prev().find("h3").text();
    console.log("Header: " + header);
    var Environment = header.split(" ")[2];
    var Instance = header.split(" ")[3];

    if (action == "Restart"){
        $('.restartButton').button('loading');
    }
    else if (action == "Start") {
        $('.startButton').button('loading');
    }
    else if (action == "Stop") {
        $('.stopButton').button('loading');
    }
    else if (action == "Rsync") {
        $('.rsyncButton').button('loading');
    }
    else if (action == "Logs") {
        $('.logsButton').button('loading');
    }

    $.ajax({
        type: "POST",
        url: "scriptRunner.php",
        data: {Environment: Environment, Instance: Instance, Action: action},
        success: function(response){ 
            $('footer').append(response);
        }
    });
};

// Add the download button to download a log file
function addDownloadButton(fileLocation) {
    $(".actionButtons").append('<a href="' + fileLocation + '" download> <button type="button" class="btn btn-secondary actionButton downloadButton"> Download </button></a>' );
}

// Reset the server action buttons
function resetButton(action){
    if (action == "Restart"){
        $('.restartButton').button('reset');
    }
    else if (action == "Start") {
        $('.startButton').button('reset');
    }
    else if (action == "Stop") {
        $('.stopButton').button('reset');
    }
    else if (action == "Rsync") {
        $('.rsyncButton').button('reset');
    }
    else if (action == "Logs") {
        $('.logsButton').button('reset');
    }
}

// Write an array of changedElement to the DB
function writeToDB(myType, myValue){
    console.log("Type: " + myType + "  ||  Value: " + myValue);
    $.ajax({
        type: "POST",
        url: "writeToDB.php",
        data: {TypeParam: myType, ValueParam: JSON.stringify(myValue), user: loggedInUser},
        success: function(response){ 
            $('footer').append(response);
        }
    });
}

// Show popover information (SSH and winSCP link)
$(function () {
    $(".pop").hover(function() {
        $(this).attr("data-content", '<a href="ssh://' + $(this).text() + '"><img class="SuperPuttyIcon" src="imgs/SuperPuTTY.png"></a><a href="winscp-sftp://' + $(this).text() + '"><img class="WinSCPIcon" src="imgs/WinSCP.png"></a>');
    });
});
     
// Make popover stay open when hovering over
$(document).ready(function(){
    $(".pop").popover({ trigger: "manual" , html: true, animation:true})
    .on("mouseenter", function () {
        var _this = this;
        $(this).popover('show');
        $(".popover").on("mouseleave", function () {
            $(_this).popover('hide');
        });
    }).on("mouseleave", function () {
        var _this = this;
        setTimeout(function () {
            if (!$(".popover:hover").length) {
                $(_this).popover("hide");
            }
        }, 300);
});
});

