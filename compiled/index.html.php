<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SPACE</title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/index.css">
  <script src="index.js"></script>

  <nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">
          <img class="ABS_Logo" src="imgs/ABS_Space_Logo.png" alt="ABS SPACE">
        </a>
      </div>
      
      <ul class="nav navbar-nav navbar-left">
        <li><a class="white_link" href="Links.html.php">Links</a></li>
      </ul>
      <div class="RightNavElements">
        <button type="button" class="btn UpdateButton"> Update </button>
        <?php include 'getUserInfo.php'; ?>
      </div>
    </div>
  </nav>
</head>

<body>
  <div class="container-fluid">

  </div>
  <?php include 'handler.php'; ?>
    </div>
    
    <!-- Server info dropdown modal -->
    <div class="modal fade" id="serverInfoModal" tabindex="-1" role="dialog" aria-labelledby="serverInfoModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content modalServerInfo">
          <div class="modal-header modalServerInfo">
            <h3 class="modal-title modalServerInfo" id="serverInfoModalLabel">Loading Information...</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body modalServerInfo">
            <p>Loading Information...</p>
          </div>
          <div class="modal-footer modalServerInfo">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- END Server info dropdown modal -->

    <!-- Instance info dropdown modal -->
    <div class="modal fade Server-Modal" id="instanceInfoModal" tabindex="-1" role="dialog" aria-labelledby="instanceInfoModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content modalInstanceInfo">
          <div class="modal-header modalInstanceInfo">
            <h3 class="modal-title modalInstanceInfo" id="instanceInfoModalLabel">Loading Information...</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body modalInstanceInfo">
            <p>Loading Information...</p>
          </div>
          <div class="modal-footer modalInstanceInfo">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- END Instance info dropdown modal -->

     <!-- Message Prompt dropdown modal -->
    <div class="modal fade Server-Modal" id="MessagePromptModal" tabindex="-1" role="dialog" aria-labelledby="MessagePromptModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content modalMessagePrompt">
          <div class="modal-header modalMessagePrompt">
            <h3 class="modal-title modalMessagePrompt" id="MessagePromptModalLabel">Enter Message</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body modalMessagePrompt">
            <label for="actionMessage">Message</label>
            <textarea class="form-control" id="actionMessage" rows="1"></textarea>
          </div>
          <div class="modal-footer modalMessagePrompt">
            <button type="button" class="btn btn-primary messageAcceptOK" data-dismiss="modal">OK</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- END Instance info dropdown modal -->


</body>
<footer class="footerClass">

</footer>

</html>