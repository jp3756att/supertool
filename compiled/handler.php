<?php
$servername = "zlt08585.vci.att.com";
$username = "admin";
$password = "admin123";
$dbname = "NonProdDB";
$conn = "";
$DBConnect = False;

error_reporting(0);

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $DBConnect = True;
}

catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
    $DBConnect = False;
}


function createArrays($DBConnection, $sqlQuery, $ArrayType){
    $InstanceProperties = explode(",", explode(" ", $sqlQuery)[1]);
    $data = $DBConnection->query($sqlQuery)->fetchAll(PDO::FETCH_GROUP);
    

    $return_list = array();
    foreach ($data as $list_id => $list_data)
    {
        if (!array_key_exists($list_id, $return_list)){
            $return_list[$list_id] = array();
        }
        //echo $list_data[0]['list_name']."<br />\n"; 
        foreach ($list_data as $row)
        {
            if (!array_key_exists($row['Environment'], $return_list[$list_id])){
            
                $return_list[$list_id][$row['Environment']] = array();
            }
        
            if ($ArrayType == "AppServer"){
                if (!array_key_exists($row['Instance'], $return_list[$list_id][$row['Environment']])){
                    
                    $return_list[$list_id][$row['Environment']][$row['Instance']] = array();
                }

                foreach ($InstanceProperties as $prop){
                    $return_list[$list_id][$row['Environment']][$row['Instance']][$prop] = $row[$prop];
                }
            }

            elseif ($ArrayType == "WebServer" || $ArrayType == "DB" || $ArrayType == "Release" || $ArrayType == "Links"){
                $arraySize = count($return_list[$list_id][$row['Environment']]);
                $return_list[$list_id][$row['Environment']][$arraySize] = array();
                foreach ($InstanceProperties as $prop){
                    $return_list[$list_id][$row['Environment']][$arraySize][$prop] = $row[$prop];
                }
            } 
        }
    }
    return $return_list;

}

$AppServerSql = 'SELECT EnvironmentGroup,Environment,Instance,Server,AdminUI_Port,RMI_Port,DRP_Port,CM_Port,Console_Port,MuteTime,PermMute,Message,Notify,DownTime,User_Updated,Rooms,ConsoleOrAdmin,HasWebpage FROM NonProd_App WHERE Show_In_Tool=1';
$AppServers = createArrays($conn, $AppServerSql, "AppServer");

$WebServerSql = 'SELECT EnvironmentGroup,Environment,Type,Instances,Server,Port,DownTime,MuteTime,PermMute,Message,Notify,Rooms,User_Updated FROM NonProd_Web';
$WebServers = createArrays($conn, $WebServerSql, "WebServer");

$DBSql = 'SELECT EnvironmentGroup,Environment,Server,Port,Name FROM NonProd_DB';
$DBServers = createArrays($conn, $DBSql, "DB");

$ReleaseSql = 'SELECT EnvironmentGroup,Environment,Scheduled_Downtimes,Chatrooms,Release_Name,Assignment FROM NonProd_Release';
$ReleaseServers = createArrays($conn, $ReleaseSql, "Release");

$WebServerLinksSql = 'SELECT EnvironmentGroup,Environment,Name,Server,Port,Extension,Type FROM NonProd_Links';
$WebServerLinks = createArrays($conn, $WebServerLinksSql, "Links");


// print '<pre>';
// print_r($ReleaseServers);
// print "</pre>";


print '<div class="row">';
print '<div class="col-md-12">';
print '<ul class="nav nav-pills EnvTabs">';

// Environment Groups
$sqlEnvGroup = 'SELECT DISTINCT EnvironmentGroup FROM NonProd_App';
//print '<script> console.log("Unique: ' . array_keys($WebServers)[0]  . '"); </script>';


$EnvironmentGroups = array_unique(array_keys($AppServers));


$first = True;

foreach ($EnvironmentGroups as $Environment){
    if ($first) {
        print '<li role="presentation" class="active ' . $Environment . 'Tab">';
        $first = False;
    }
    else{
        print '<li role="presentation" class="' . $Environment . 'Tab">';
    }
    print '<a href="#' . $Environment . '" data-toggle="tab"> ' . $Environment . ' </a>';
    print '</li>';
}

print '</ul>';

// Environments
print '<div class="tab-content">';
$first = True;
foreach ($EnvironmentGroups as $Environment){
    if ($first) {
        print '<div id="'. $Environment . '" class="tab-pane fade in active">';
        $first = False;
    }
    else {
        print '<div id="'. $Environment . '" class="tab-pane fade in">';
    }
    buildTables($Environment, $conn, $AppServers, $WebServers, $DBServers, $ReleaseServers, $WebServerLinks);
    print '</div>';
}
print '</div>';
print '</div>';
print '</div>';

$conn = null;


// ------------------------------------- createWebServerRow -------------------------------------
// ----------------------------------------------------------------------------------------------
// Creates row in web server table
// Parameters: The current server to create row for
function createWebServerRow($currentServer) {
    print '<tr class="' . $currentServer['Type'] . $currentServer['Environment'] . $currentServer['Instances'] . ' ServerRow">';
        print '<td>' . $currentServer['Instances'] . '</td>' ;
        print '<td>';
            createServerLink($currentServer['Server']);
        print '</td>';
        print '<td><input type="button" class="DeetsButton btn btn-xs" value="DIM"/></td>';
    print '</tr>';
    global $accountType;
    if ($accountType == "admin"){
        print '<tr class="' . $currentServer['Type'] . $currentServer['Environment'] . $currentServer['Instances'] . 'Details detailsRow">';
            print '<td colspan="3">';
                print '<script>setTabColors("' . $currentServer['Environment'] . '","' . $currentServer['DownTime'] . '", "' . $currentServer['PermMute'] . '","' . $currentServer['MuteTime'] . '",' . this . ')</script>';
                print '<table class="AppDetailsTable table table-hover">';
                    print '<tr>';
                        print '<td class="AppDetailsTable_Type"> Down Time </td>';
                        print '<td>' . $currentServer['DownTime'] . '</td>';
                        print '<td class="AppDetailsTable_Type MuteUntilInput Updatable"> Mute Until </td>';
                        print '<td><input type="datetime-local"/></td>';
                    print '</tr>';
                    print '<tr>';
                        print '<td class="AppDetailsTable_Type PermMuteButton Updatable"> Mute Perm </td>';
                        print '<td><input type="checkbox" class="PermMuteButton"/></td>';
                        print '<td class="AppDetailsTable_Type NotifyButton Updatable"> Notify </td>';
                        print '<td><input type="checkbox" data-group-cls="btn-group-sm"/></td>';
                    print '</tr>';
                    print '<tr>';
                        print '<td class="AppDetailsTable_Type MessageInput Updatable"> Message </td>';
                        print '<td><input type="text" class="MessageInput"/></td>';
                        print '<td class="AppDetailsTable_Type MessageInput Updatable"> Rooms </td>';
                        print '<td><input type="text class="RoomsInput"/></td>';
                    print '</tr>';
                print '</table>';
            print '</td>';
        print '</tr>';
    }
}
// ----------------------------------------------------------------------------------------------


// -------------------------------------- createServerLink --------------------------------------
// Creates popover server link
// Parameters: The name of the server to display / link
function createServerLink($server) {
    print '<a class="pop ServerLink GrayLink" data-toggle="modal" data-target="#serverInfoModal" data-placement="bottom">' . $server . '</a>';
    #print '<a class="pop ServerLink GrayLink" data-container="body" data-toggle="popover" data-placement="bottom" data-content="">' . $server . '</a>';
    #print '<button type="button" class="btn btn-link ServerInfoButton" data-toggle="modal" data-target="#serverInfoModal"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></button>';
}
// ----------------------------------------------------------------------------------------------

// ---------------------------------------- buildTables -----------------------------------------
// Builds the tables for each instance
// Parameters: The environment group name, The DB connection
function buildTables($Env, $conn, $AppServers, $WebServers, $DBServers, $ReleaseServers, $WebServerLinks) {
    
    // Print out Environment tabs (PQA1, PQA2, BCE1, BCE2, etc)
    print '<ul class="nav nav-pills InstanceTabs">';
    
    // Query for Environments
    $sql = 'SELECT DISTINCT Environment FROM NonProd_App WHERE Environment LIKE "%' . $Env . '%"';
    $Environments = array_unique(array_keys($AppServers[$Env]));

    foreach ($Environments as $Environment) {
        print '<li role="presentation" class="' . $Environment . 'Tab' . '"><a href="#' . $Environment . '" data-toggle="tab">' . $Environment . '</a></li>';
    }
    print '</ul>';
    
    // print Environment details
    print '<div class="tab-content">';
    foreach ($Environments as $Environment) {
        print '<div id="' . $Environment . '" class="tab-pane fade in">'; //tab-pane
        
        // Web Table
        $sqlWebTableHTTP = 'SELECT * FROM NonProd_Web WHERE Environment ="' . $row['Environment'] . '" AND Type = "HTTP"';
        $sqlWebTableHTTPS = 'SELECT * FROM NonProd_Web WHERE Environment ="' . $row['Environment'] . '" AND Type = "HTTPS"';
        
        $sqlWebTableHTTPLinks = 'SELECT * FROM NonProd_Links WHERE Environment ="' . $row['Environment'] . '" AND Type = "HTTP"';
        $sqlWebTableHTTPSLinks = 'SELECT * FROM NonProd_Links WHERE Environment ="' . $row['Environment'] . '" AND Type = "HTTPS"';

        $sqlReleaseInfo = 'SELECT * FROM NonProd_Release WHERE Environment ="' . $row['Environment'] . '"';
        
        // Release infomation header
        print '<div class="row">'; //row
            print '<div class="col-md-1">';
            print '</div>'; // col-md-1
            print '<div class="col-md-10">';
                print '<table class="table ReleaseTable">'; // table

                    print '<th> Release </th><th> Scheduled Downtimes </th><th> Chatroom </th><th> Assignment </th>';

                    print '<tr>';
                    

                    print '<td>' . $ReleaseServers[$Env][$Environment][0]['Release_Name'] . '</td>';
                    print '<td>' . $ReleaseServers[$Env][$Environment][0]['Scheduled_Downtimes'] . '</td>';
                    print '<td>' . $ReleaseServers[$Env][$Environment][0]['Chatrooms'] . '</td>';
                    print '<td>' . $ReleaseServers[$Env][$Environment][0]['Assignment'] . '</td>';

                    print '</tr>';
                print '</table>';

            print '</div>';
        print '</div>';


        print '<div class="row">'; //row
        print '<div class="col-md-1">';
        print '</div>'; // col-md-1
        print '<div class="col-md-5">'; // col-md-5
        print '<table class="table table-hover WebTable">'; // table
        print '<tr><td colspan="3" class="WebTableHeader">Web</td></tr>';
        
        print '<tr><td colspan="3" class="WebTableHTTPSHeader">HTTPS</td></tr>';
        print '<tr>';
                print '<th> Instances </th><th> URL </th><th></th>';
        print '</tr>';
        foreach ($WebServers[$Env][$Environment] as $webserver) {
            createWebServerRow($webserver);
        }
        
        print '<tr><td colspan="3" class="WebTableHTTPSHeader">HTTPS Links</td></tr>';
        print '<th> Name </th><th> URL </th><th></th>';
        

        foreach ($WebServerLinks[$Env][$Environment] as $link) {
            print '<tr>';
                print '<td>' . $link['Name'] . '</td>' ;
                $HTTPSLink = 'https://' . $link['Server'] . ":" . $link['Port'] . '/' . $link['Extension'];
                print '<td><a href="' . $HTTPSLink . '" target="_blank">' . $HTTPSLink . '</a></td>';
                print '<td></td>';
            print '</tr>';

        }
        
        
        print '</table>'; //table
        print '</div>'; // col-md-5
        
        
        // App Table
        $sqlAppTable = 'SELECT * FROM NonProd_App WHERE Environment ="' . $Environment . '"';
        
        print '<div class="col-md-5">'; // col-md-5
        print '<table class="table table-hover AppTable">'; // table
        print '<tr><td colspan="8" class="AppTableHeader">App</td></tr>';
        print '<tr>';
            print '<th> Instance </th><th> Server </th><th> AdminUI </th><th> Console </th><th> RMI </th><th> DRP </th><th> FileDeployment </th><th> DIM </th>';
        print '</tr>';
        foreach ($AppServers[$Env][$Environment] as $innerrow) {
            print '<tr class="' . $innerrow['Instance'] . $innerrow['Environment'] . ' ServerRow">';
                print '<td class="InstanceCol">';
                print '<div><a class="GrayLink InstanceLink" data-toggle="modal" data-target="#instanceInfoModal">' . $innerrow['Instance'] . '</a></div>';
                //    print '<button type="button" class="btn btn-link InstanceInfoButton" data-toggle="modal" data-target="#serverInfoModal"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></button>';
                print '</td>' ;
                print '<td>';
                    #print '<a class="pop" data-container="body" data-toggle="popover" data-placement="bottom" data-content="">' . $innerrow['Server'] . '</a>';
                    createServerLink($innerrow['Server']);   
                print '</td>';
                print '<td><a href="http://' . $innerrow['Server'] . ':' . $innerrow['AdminUI_Port'] .'/dyn/admin" class="GrayLink" target="_blank">' . $innerrow['AdminUI_Port'] . '</a></td>';
                print '<td><a href="http://' . $innerrow['Server'] . ':' . $innerrow['Console_Port'] .'/console" class="GrayLink" target="_blank">' . $innerrow['Console_Port'] . '</a></td>';
                print '<td>' . $innerrow['ACC_Port'] . '</td>';
                print '<td>' . $innerrow['DRP_Port'] . '</td>';
                print '<td>' . $innerrow['FileDeployment_Port'] . '</td>';
                print '<td><input type="button" class="DeetsButton btn btn-xs" value="DIM"/></td>';
            print '</tr>';
            global $accountType;
            if ($accountType == "admin"){
            print '<tr class="' . $innerrow['Instance'] . $innerrow['Environment'] . 'Details detailsRow">';
                print '<td colspan="9">';
                    print '<script>setTabColors("' . $innerrow['Environment'] . '","' . $innerrow['DownTime'] . '", "' . $innerrow['PermMute'] . '","' . $innerrow['MuteTime'] . '",' . this . ')</script>';
                    print '<table class="AppDetailsTable table table-hover">';
                        print '<tr>';
                            print '<td class="AppDetailsTable_Type"> Down Time </td>';
                            print '<td>' . $innerrow['DownTime'] . '</td>';
                            print '<td class="AppDetailsTable_Type"> Mute Until </td>';
                            print '<td><input type="datetime-local" class="MuteUntilInput Updatable"/></td>';
                        print '</tr>';
                        print '<tr>';
                            print '<td class="AppDetailsTable_Type"> Mute Perm </td>';
                            print '<td><input type="checkbox" class="PermMuteButton Updatable"/></td>';
                            print '<td class="AppDetailsTable_Type"> Has Webpage </td>';
                            print '<td><input type="checkbox" class="WebpageButton Updatable"/></td>';
                            
                        print '</tr>';
                        print '<tr>';
                            print '<td class="AppDetailsTable_Type"> Message </td>';
                            print '<td><input type="text" class="MessageInput Updatable"/></td>';
                            print '<td class="AppDetailsTable_Type"> URL Check </td>';
                            print '<td>';
                                print '<select class="DownCheckSelect form-control DownCheckURL_Dropdown Updatable" id="sel1">';
                                    print '<option>Console</option>';
                                    print '<option>AdminUI</option>';
                                print '</select>';
                            print '</td>';
                        print '</tr>';
                        print '<tr>';
                            print '<td class="AppDetailsTable_Type"> Notify </td>';
                            print '<td><input type="checkbox" data-group-cls="btn-group-sm" class="NotifyButton Updatable"/></td>';
                            print '<td class="AppDetailsTable_Type"> Rooms </td>';
                            print '<td><input type="text" class="RoomsInput Updatable"/></td>';
                        print '</tr>';
                        print '<script>SetDIMInfo('. json_encode($innerrow) . ');</script>';
                    print '</table>';
                print '</td>';
            print '</tr>';
            }
            
        }
        print '</table>'; //table

        // DB Table
        $sqlDBTable = 'SELECT * FROM NonProd_DB WHERE Environment ="' . $Environment . '" LIMIT 1';

        print '<table class="table table-hover DBTable">'; // table
        print '<tr><td colspan="3" class="DBTableHeader">DB</td></tr>';
        print '<tr>';
            print '<th> Server </th><th> Port </th><th> SID </th>';
        print '</tr>';
        foreach ($DBServers[$Env][$Environment] as $innerrow) {
            print '<tr>';
            print '<td>';  
                createServerLink($innerrow['Server']);
            print '</td>';
            print '<td>' . $innerrow['Port'] . '</td>';
            print '<td>' . $innerrow['Name'] . '</td>';
            print '</tr>';
        }
        print '</table>';
        print '</div>'; // col-md-5
        
        
        print '</div>'; // row
        
        print '</div>'; // tab-pane
    }
    print '</div>';
}
// ----------------------------------------------------------------------------------------------

?>