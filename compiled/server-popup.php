<?php
$servername = "zlt08585.vci.att.com";
$username = "admin";
$password = "admin123";
$dbname = "NonProdDB";
$conn = "";
$DBConnect = False;

$Environment = $_POST['Environment'];
$Instance = $_POST['Instance'];
$accountType = $_POST['AccountType'];

# Try to connect to the DB
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $DBConnect = True;
}

# If DB connection fails
catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
    $DBConnect = False;
}

# Get configurations for current instance
$sqlConfigurations = 'SELECT * FROM NonProd_Configuration WHERE Environment = "' . $Environment . '" AND Instance = "' . $Instance . '"';

print '<div class="actionButtons">';
print '<script> console.log("accountType:' . $accountType . '") </script>';

# Display the start, stop, restart, and rsync buttons if you are admin

if ($accountType == "admin") {

    print '<button type="button" class="btn btn-success actionButton startButton" data-toggle="modal" data-target="#MessagePromptModal" onclick="promptForMessage(\'Start\')" data-loading-text="Working...">Start</button>';
    print '<button type="button" class="btn btn-danger actionButton stopButton" data-toggle="modal" data-target="#MessagePromptModal" onclick="promptForMessage(\'Stop\')" data-loading-text="Working...">Stop</button>';
    print '<button type="button" class="btn btn-primary actionButton restartButton" data-toggle="modal" data-target="#MessagePromptModal" onclick="promptForMessage(\'Restart\')" data-loading-text="Working...">Restart</button>';
    print '<button type="button" class="btn btn-warning actionButton rsyncButton" data-toggle="modal" data-target="#MessagePromptModal" onclick="promptForMessage(\'Rsync\')" data-loading-text="Working...">Rsync</button>';

}

if ($accountType == "admin" || $accountType == "dev"){

    # Display the log button if you are anyone
    print '<button type="button" class="btn btn-secondary actionButton logsButton" data-loading-text="Working..." onclick="actionOnServer(\'Logs\')">Logs</button>';

}
    print '</div>';

print '<div class="ConfigTableDiv">';
print '<table class="table table-bordered ConfigTable">';

# Print out the configurations
foreach ($conn->query($sqlConfigurations) as $config) {
    print '<tr>';
        print '<td class="boldColumn">' . $config['Property'] . '</td>';
        print '<td class="breakableColumn">' . $config['Value'] . '</td>';
    print '</tr>';
}

print '</table>';
print '</div>';

?>