<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>NonProd SuperTool</title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/Links.css">
  
  <nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">
          <img class="ABS_Logo" src="imgs/ABS_Space_Logo.png" alt="ABS SPACE">
        </a>
      </div>

      <ul class="nav navbar-nav navbar-left">
        <li><a class="white_link" href="index.html.php">SuperTool</a></li>
      </ul>
      <div class="RightNavElements">
        <?php print '<h4 class="HelloText"> Hello ' . $_SERVER['PHP_AUTH_USER'] . '</h4>'; ?>
        <?php print '<script> loggedInUser = "' . $_SERVER['PHP_AUTH_USER'] . '"</script>'; ?>
      </div>
    </div>
  </nav>
</head>

<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td class="TableHeader" colspan="10">General</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="http://zlpv2267.vci.att.com:7000/arsys/forms/zlpv9054/EEM:EnterpriseEnvironmentManagementConsole/" target="_blank">Remedy</a></td>
              <td><a href="https://tspace.web.att.com/communities/service/html/communityview?communityUuid=22ce92ec-a4df-43c9-ad5d-810a7de20b0d#fullpageWidgetId=W7ec7e7b72c86_407d_b8b5_e918413367b5&file=fdcecc8a-6669-49a6-b6a7-85313800cbbb" target="_blank">ATG BSD</a></td>
              <td><a href="https://wiki.web.att.com/pages/viewpage.action?pageId=531550234#MyDeck--1156208761" target="_blank">Premier Contacts</a></td>
              <td><a href="https://wiki.web.att.com/display/1246/ABS+TEAM+HANDOVER" target="_blank">Team Handover</a></td>
              <td><a href="https://zlt05041.vci.att.com:9911/index.jsp" target="_blank">Dynatrace</a></td>
              <td><a href="https://zld03979.vci.att.com/api/v1/proxy/namespaces/kube-system/services/kubernetes-dashboard/" target="_blank">Kubernetes</a></td>
              <td><a href="https://ved.web.att.com/VEDAdmin/" target="_blank">VED</a></td>
              <td><a href="http://ushportal.it.att.com/step3selfhelp.cfm?home=ush&app=575&prob=36443&subprob=0" target="_blank">Linux Admin Support</a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-md-1"></div>
    </div>
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-5">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td class="TableHeader" colspan="5">eBiz</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="https://wiki.web.att.com/display/SAB2B/Dallas+Cloud+Environment+Details" target="_blank">Wiki</a></td>
              <td><a href="https://wiki.web.att.com/pages/viewpage.action?title=Environment+Details+-+Trinity&spaceKey=SAB2B" target="_blank">Trinity Wiki</a></td>
              <td><a href="https://codecloud.web.att.com/login?next=/projects/ST_ATTEBIZ/" target="_blank">CodeCloud</a></td>
              <td><a href="http://zlt11445.vci.att.com:8080/nagios" target="_blank">Nagios</a></td>
              <td><a href="http://zltv8762.vci.att.com:18080/jenkins/" target="_blank">Jenkins</a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-md-5">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td class="TableHeader" colspan="5">Premier</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="https://wiki.web.att.com/display/SAB2B/Environment+Details+-+New" target="_blank">Wiki</a></td>
              <td><a href="https://wiki.web.att.com/pages/viewpage.action?pageId=531550234#MyDeck-1998935843" target="_blank">U2L Wiki</a></td>
              <td><a href="https://wiki.web.att.com/display/SAB2B/QA+Cache+Invalidation" target="_blank">Cache Invalidation</a></td>
              <td><a href="http://zlt08586.vci.att.com:8080/nagios" target="_blank">Nagios</a></td>
              <td><a href="http://zld02446.vci.att.com:18080/jenkins/view/1706_Dashboard/" target="_blank">Jenkins</a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-5">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td class="TableHeader" colspan="5">Documents</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="#" target="_blank">Karaf Setup</a></td>
              <td><a href="#" target="_blank">U2L Setup</a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-md-5">
        <table class="table table-bordered">
          <thead>
            <tr>
              <td class="TableHeader" colspan="5">Miscellaneous</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="http://fuse.web.att.com/" target="_blank">Fu$e</a></td>
              <td><a href="https://cmpm.web.att.com/" target="_blank">CMPM</a></td>
              <td><a href="https://toolsssl.sbc.com/" target="_blank">ITSERVICE Password Reset</a></td>
              <td><a href="https://aod.web.att.com/dashboard" target="_blank">Arcade On Demand (AOD)</a></td>
            </tr>
            <tr>
              <td><a href="https://ieds.it.att.com/ieds/pages/home.cfm" target="_blank">IEDS</a></td>
              <td><a href="http://envtracker.it.att.com/index.php?view=applications&name=CSI&ENV_NAME=CSI_Q29C" target="_blank">ACSITOOLS CSI Tracker</a></td>
              <td><a href="http://servicedesk.it.att.com/toolkit/cdt/" target="_blank">Service Desk (AT&T Toolkit)</a></td>
              <td><a href="https://ecorplb.sbc.com/irj/portal" target="_blank">eCorp</a></td>
            </tr>
            <tr>
              <td><a href="https://www.e-access.att.com/stdm/" target="_blank">RSA Token</a></td>
              <td><a href="https://www.e-access.att.com/src/" target="_blank">Security Request Center (SRC)</a></td>
              <td><a href="https://tspace.web.att.com/files/app/file/9fc45d7d-4cb4-4f92-a7ca-bc86f7c58b1f" target="_blank">ITKO URLs</a></td>
              <td><a href="http://softwarestore.sbc.com/" target="_blank">Software Store</a></td>
            </tr>
            <tr>
              <td><a href="http://alpd307.aldc.att.com:10000/swmGuiWeb/" target="_blank">SWM GUI</a></td>

            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-md-1"></div>
    </div>
  </div>
</body>
<footer class="footerClass">

</footer>

</html>
