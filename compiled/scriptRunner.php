<?php
    $Environment = $_POST['Environment'];
    $Instance = $_POST['Instance'];
    $Action = $_POST['Action'];
    
    $output = "";

    # If this is a PQCE environment, you can perform actions
    if (substr($Environment, 0, 4) == "PQCE") {

        //print '<script> alert(\'' . $Environment . $Instance . $Action . '\') </script>'; 

        # If you want to get the logs
        if ($Action == "Logs") {

            # Call logGetter script
            exec("/home/jenkins/python3.6/python /home/jenkins/scripts/Log_Fetch.py -e " . $Environment . " -i " . $Instance, $output);

            # Name of the file that was downloaded from logGetter script
            $file = $Environment . "-" . $Instance . "-std.out";

            if (file_exists($file)) {

                # Add the button to download to client
                print '<script> addDownloadButton("' . $file . '") </script>';

            }

            print_r($output);
            print '<script> console.log(\'' . $output[0] . '\') </script>'; 

            # Reset the Log Button
            print '<script> resetButton("' . $Action . '") </script>';


        }

        # Start the instance
        else if ($Action == "Start") {
            $output = exec("/home/jenkins/python3.6/python /home/jenkins/test/PQCE_Environment_Action.py -e " . $Environment . " -i " . $Instance . " -a start");
            
            print '<script> resetButton("' . $Action . '") </script>';
        }

        # Stop the instance
        else if ($Action == "Stop") {
            $output = exec("/home/jenkins/python3.6/python /home/jenkins/test/PQCE_Environment_Action.py -e " . $Environment . " -i " . $Instance . " -a stop");
            print '<script> resetButton("' . $Action . '") </script>';
        }

        # Restart the instance
        else if ($Action == "Restart") {
            $output = exec("/home/jenkins/python3.6/python /home/jenkins/test/PQCE_Environment_Action.py -e " . $Environment . " -i " . $Instance . " -a restart");
            print '<script> resetButton("' . $Action . '") </script>';
        }

        # Rsync the instance
        else if ($Action == "Rsync") {
            $output = exec("/home/jenkins/python3.6/python /home/jenkins/test/PQCE_Environment_Action.py -e " . $Environment . " -i " . $Instance . " -a rsync");
            print '<script> resetButton("' . $Action . '") </script>';
        }

        # Alert the output of the executed script
        print '<script> alert(\'' . $output . '\') </script>'; 
    }

    # If not a PQCE environment
    else {
        print '<script> alert("These functions currently only work for PQCE") </script>';  
    }


?>