<?php
$servername = "zlt08585.vci.att.com";
$username = "admin";
$password = "admin123";
$dbname = "NonProdDB";
$conn = "";
$DBConnect = False;

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $DBConnect = True;
}

catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
    $DBConnect = False;
}

$WebServerSql = 'SELECT EnvironmentGroup,Environment,Type,Instances,Server,Port,DownTime,MuteTime,PermMute,Message,Notify,Rooms,User_Updated FROM NonProd_Web';
$InstanceProperties = explode(",", explode(" ", $WebServerSql)[1]);
$ArrayType = "WebServer";
$data = $conn->query($WebServerSql)->fetchAll(PDO::FETCH_GROUP);

//print '<pre>';
//print_r ($data);
//print '</pre>';


$return_list = array();
foreach ($data as $list_id => $list_data)
{
    if (!array_key_exists($list_id, $return_list)){
        $return_list[$list_id] = array();
    }
    //echo $list_data[0]['list_name']."<br />\n"; 
    foreach ($list_data as $row)
    {
        if (!array_key_exists($row['Environment'], $return_list[$list_id])){
        
            $return_list[$list_id][$row['Environment']] = array();
        }
    
        if ($ArrayType == "AppServer"){
            $arraySize = 0;
            if (!array_key_exists($row['Instance'], $return_list[$list_id][$row['Environment']])){
                
                $return_list[$list_id][$row['Environment']][$row['Instance']] = array();
                $arraySize = count($return_list[$list_id][$row['Environment']][$row['Instance']]);
                $return_list[$list_id][$row['Environment']][$row['Instance']][$arraySize] = array();
            }
            else{
                $arraySize = count($return_list[$list_id][$row['Environment']][$row['Instance']]);
                $return_list[$list_id][$row['Environment']][$row['Instance']][$arraySize] = array();
            }

            foreach ($InstanceProperties as $prop){
                $return_list[$list_id][$row['Environment']][$row['Instance']][$arraySize][$prop] = $row[$prop];
            }
        }

        elseif ($ArrayType == "WebServer" || $ArrayType == "DB" || $ArrayType == "Release" || $ArrayType == "Links"){
            $arraySize = count($return_list[$list_id][$row['Environment']]);
            $return_list[$list_id][$row['Environment']][$arraySize] = array();
            foreach ($InstanceProperties as $prop){
                $return_list[$list_id][$row['Environment']][$arraySize][$prop] = $row[$prop];
            }
        } 
    }
}
print '<pre>';
print_r ($return_list);
print '</pre>';


?>