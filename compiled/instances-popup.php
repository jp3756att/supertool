<?php
$servername = "zlt08585.vci.att.com";
$username = "admin";
$password = "admin123";
$dbname = "NonProdDB";
$conn = "";
$DBConnect = False;

$ServerName = $_POST['ServerName'];

function print_IEDS_Row($prop, $value){
    print '<tr>';
    print '<td class="boldColumn">' . $prop . '</td>';
    print '<td>' . $value . '</td>';
    print '</tr>';
}

# Try to connect to the DB
try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $DBConnect = True;
}

# If DB connection fails
catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
    $DBConnect = False;
}

# Get all app isntances on server
$sqlInstances = 'SELECT DISTINCT Environment,Instance FROM NonProd_App WHERE Server="' . $ServerName . '"';
print '<h4>Instances on this server:</h4>';
print '<table class="table table-bordered AppTable">';

$itemNum = 0;   # Counter for last row

# Loop through all instances
foreach ($conn->query($sqlInstances) as $Instance) {

    if (($itemNum % 3) == 0) {
        print '<tr>';
    }

    print '<td>' . $Instance['Environment'] . '-' . $Instance['Instance'] . '</td>';

   
    if (($itemNum % 3) == 2) {
        print '</tr>';
    }
     $itemNum = $itemNum + 1;
    
}

$sqlWebInstances = 'SELECT DISTINCT Environment,Type FROM NonProd_Web WHERE Server="' . $ServerName . '"';

# Loop through all web instances on server
foreach ($conn->query($sqlWebInstances) as $WebInstance) {
    if (($itemNum % 3) == 0) {
        print '<tr>';
    }
    print '<td>' . $WebInstance['Environment'] . '-' . $WebInstance['Type'] . '</td>';

   

    if (($itemNum % 3) == 2) {
        print '</tr>';
    }
     $itemNum = $itemNum + 1;
    
}
if (($itemNum % 3) != 0) {
        print '</tr>';
}

print '</table>';

print '<h4>Server Specifications:</h4>';

print '<table class="table table-bordered IEDSTable">';
$IEDS_Hostname = explode(".", $ServerName)[0];
$sqlIEDSInstances = 'SELECT SERVER_ID,RAM_MEMORY,CPU_INSTALLED,PHYSICAL_CPU,DISK_STORAGE,OS_RELEASE,OP_SYS,PATCHED_DATE,CITY,STATE FROM NonProd_IEDS WHERE SYS_NAME="' . $IEDS_Hostname . '" LIMIT 1';

# Loop through IEDS configs
foreach ($conn->query($sqlIEDSInstances) as $IEDSInstance) {
    print_IEDS_Row('RAM', $IEDSInstance['RAM_MEMORY']);
    print_IEDS_Row('CPU Installed', $IEDSInstance['CPU_INSTALLED']);
    print_IEDS_Row('Physical CPU', $IEDSInstance['PHYSICAL_CPU']);
    print_IEDS_Row('Disk Storage', $IEDSInstance['DISK_STORAGE']);
    print_IEDS_Row('OS', $IEDSInstance['OP_SYS']);
    print_IEDS_Row('OS Release', $IEDSInstance['OS_RELEASE']);
    print_IEDS_Row('City', $IEDSInstance['CITY']);
    print_IEDS_Row('State', $IEDSInstance['STATE']);

    print '</table>';
    print '<div class="MoreIEDSInfo">';
    print '<a href="https://ieds.it.att.com/ieds/pages/sysmain.cfm?sid=' . $IEDSInstance['SERVER_ID'] . '" target="_blank">See more IEDS information</a>';
    print '</div>';

}

?>